\documentclass[12pt]{article}
\usepackage[shortlabels]{enumitem}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tikz}
\usepackage{comment}
\usepackage{url}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\graphicspath{ { . } }
\usepackage[margin=1in]{geometry}



\begin{document}
\subsection{Appendix -- The Sex Variable}








\subsubsection{Sex in BRFSS by Year}
\paragraph{2014 and 2015}
Prior to 2016, BRFSS interviewers were explicitly instructed to infer respondent sex by the sound of their voice. The questionnaires provided to interviewers in the 2014 and 2015 surveys read simply ``Indicate sex of respondent. \textbf{Ask only if necessary.}'' Interviewers inferred sex primarily from the respondent's voice, although if an interviewer called a landline, and the person that was ultimately interviewed was not the person that picked up phone, the respondent's sex could also be inferred from the household demographic information provided by the resident who picked up the phone. 
\paragraph{2016}
Beginning in 2016, the questionnaire instructs interviewers to explicitly ask respondents their sex. The sex question in the 2016 questionnaire read as follows:
\begin{center}
\textbf{Are you ...} \\
\begin{table}[h]
\centering
\begin{tabular}{ll}
1 & Male    \\
2 & Female  \\
9 & Refused
\end{tabular}
\end{table}
\end{center} 


It included a note for the interviewer reading:
\begin{quote}
Note: This may be populated from information derived from screening, household enumeration. However, interviewer should not make judgement on sex of respondent.
\end{quote}
This can be interpreted to mean that interviewers should no longer infer the respondent's sex based on their voice, but that for landline calls, if the sex of the respondent could be inferred from the information provided by the person who picked up the phone, that information could be used to populate this field. We show in Section \ref{interpretation} that there is reason to believe that interviewers were not complying with this note in the questionnaire, and that they continued to infer respondent sex via voice. 
\paragraph{2017}
In the 2017 questionnaire, the text of the question was unchanged, but a boldface, all-caps note was added below it: \\
\begin{quote}
\textbf{INTERVIEWER NOTE: THIS QUESTION MUST BE ASKED EVEN IF INTERVIEWER HAD PREVIOUSLY ENTERED SEX IN THE SCREENING QUESTIONS. IT WILL NOT BE ASKED OF PERSONS WHO HAVE SELF-IDENTIFIED SEX IN LL HOUSEHOLD ENUMERATION.}
\end{quote}
This was followed by another note, presumably intended to be read by the CATI (computer aided telephone interview) software implementers: 
\begin{quote}
\textbf{[CATI NOTE: THISQUESTION}[sic] \textbf{MAY BE POPULATED BY LANDLINE HOUSEHOLD ENUMERATION ONLY. IT MAY NOT BE POPULATED BY INTERVIEWER ASSIGNMENT OF SEX DURING THE SCREENING FOR CELL PHONE OR PERSONS LIVING IN COLLEGE HOUSING]}
\end{quote}
The new interviewer note indicated that interviewers were no longer allowed to infer respondent sex from the household demographic answers provided by members of the household other than the respondent. Unless the respondent personally told the interviewer their sex during the screening portion of a call to a landline, the question was to be explicitly asked of the respondent. The CATI note called for the software to require that sex be explicitly entered, regardless of answers in the screening section of the survey, if the respondent was on a cell phone or lived in college housing. This indicates that the CDC was increasingly aware of problems with the sex variable, and that steps were being taken to ensure that interviewers elicited respondent sex rather than infer it. However, in Section \ref{interpretation}, we again show that there is reason to believe that interviewers continued to infer sex via voice in 2017 to the same extent that they did prior to 2016.  
\paragraph{2018}
\par In 2018, sex was asked of all respondents. States could elect to use one of two formats for the sex question. Format 1 reads: ``What is your sex?'' Format 2 reads: ``What was your sex at birth? Was it... '' The possible responses are: \\
\begin{table}[h]
\centering
\begin{tabular}{ll}
1 & Male    \\
2 & Female  \\
7 & Don't know / Not sure \\
9 & Refused
\end{tabular}
\end{table} 
If Format 2 is used, interviewers are directed to read ``1 Male, 2 Female'' but not to read ``7 Don't know / Not sure, 9  Refused''. The codebook and questionnaire do not indicate which states elected to use which format, and the data provided does not provide any indication of which format of the question respondents were asked.  
\paragraph{2019 to 2021}
In 2019, 2020 and 2021 sex was supposed to be asked of all respondents explicitly, and the question returned to a single format. The question read ``Are you male or female?''. The possible values were the same as in 2018, but unlike in 2018, the interviewer was directed to terminate the interview on responses other than `male' and `female'. An optional Sex at Birth module was introduced and implemented in 12 states. It added a question reading ``What was your sex at birth? Was it male or female?'' The possible responses were again 1 Male, 2 Female, 7 Don't know / Not sure, and 9 Refused. Unlike the standard sex question, a response of 7 or 9 did not prompt the interviewer to terminate the interview.
An interviewer note for this question read: 
\begin{quote}
This question refers to the original birth certificate of the respondent. It does not refer to amended birth certificates. 
\end{quote}
Increased attention to the sex variable on the part of the CDC seems to finally pay off in 2019. As we show in \ref{interpretation}, there is reason to believe that interviewers actually began to elicit respondent sex. 
\paragraph{Sex Variable Names}
The CDC distributes BRFSS data in SAS transport files. The survey questions correspond to SAS variables as follows: \\
\begin{itemize}
\item In 2014 and 2015 the variable is called \texttt{SEX} and corresponds to the interviewer's inference of the respondent's sex. 
\item In 2016, the variable is called \texttt{SEX} and is meant to correspond to either the respondent's self-identified sex, or what the interviewer inferred from information provided by the person who answered the phone. There is reason to believe that sex was still inferred a majority of the time  (see Section \ref{interpretation}).
\item In 2017,  the variable is called \texttt{SEX} and is meant to correspond to the respondent's self-identified sex, but was likely still inferred a majority of the time.
\item In 2018, the variable is called \texttt{SEX1}, regardless of which format of the question was asked, and is meant to correspond to the respondent's self-identified sex, but was likely still inferred a majority of the time.
\item In 2019, 2020, and 2021, variables \texttt{LANDSEX}, \texttt{CELLSEX}, and \texttt{COLGSEX} are used for self-identified sex depending on whether the respondent was reached via landline, via cell phone, or lived in college housing, respectively. 
\begin{itemize}
\item The text of the question is the same regardless of which variable is used.
\item \texttt{LANDSEX}, \texttt{CELLSEX}, and \texttt{COLGSEX} are combined into \texttt{SEXVAR}.
\item If asked, \texttt{BIRTHSEX} is used for sex at birth. 
\item Variable \texttt{\textunderscore SEX} is set equal to \texttt{BIRTHSEX} if \texttt{BIRTHSEX} was asked, otherwise it is set equal to \texttt{SEXVAR}.
\item Section \ref{interpretation} shows why, for the first time, this is more likely to be elicited sex rather than inferred sex.
\end{itemize}
\end{itemize} 
\subsubsection{Interpretation} \label{interpretation}
\begin{figure}
\begin{center}
\includegraphics[width=6in]{miscodedbyyear}
\caption{Share of trans BRFSS respondents with miscoded sex by year. Note: respondents in 2022 were part of the 2021 survey.}
\label{fig:miscodedbyyear}
\end{center}
\end{figure}
BRFSS data has a persistent quirk in the way that transgender respondent sex is reported. Around a third of the trans BRFSS respondents have their sex coded as the sex typically associated with their gender, which according to the current academic definitions of the words `sex' and `gender' should never be the case for a trans person. That is to say, a significant number of trans men are coded as male, and a significant number of trans women are coded as female. Figure \ref{fig:miscodedbyyear} shows the share of trans BRFSS respondents whose sex is coded `wrong' by survey year. We observe that the number of trans respondents whose sex was reported `wrong' was relatively consistent from 2014 through 2018, jumped significantly in 2019, and stayed at that elevated level through the 2021 survey. We interpret this jump to represent a large increase in the rate at which BRFSS respondents were explicitly asked their sex. This jump, in conjunction with the increasing urgency of the questionnaire interviewer notes, followed by the redesign of the sex variable in 2019 and the introduction of \texttt{BIRTHSEX} show us that the CDC was aware that the sex variable was a problem, and that interviewers were often not asking respondents their sex until 2019. This means that prior to 2019, sex was primarily inferred via voice. 

The 2018 survey is unique with respect to the sex variable. In 2018, some states used an alternative format for the sex question, reading ``What was your sex at birth?'' rather than simply ``What is your sex?''. The BRFSS data, codebook, and questionnaire for that year provide no indication of when and where that alternative format was used. This uncertainty makes the 2018 survey unusable for our gender perception measure. As explained in the last paragraph, years after 2018 have elicited rather than inferred sex, so our research considers only the 2014 through 2017 surveys. 

Besides inference and elicitation from the respondent, the sex variable could also be populated during the household enumeration phase of an interview conducted via land line.  Prior to 2017, interviewers were allowed to populate the sex variable for the respondent based on statements made by a non-respondent who answered a landline, although like inference, we have no reason to believe this practice stopped until 2019. Only around one in four trans people surveyed prior to 2019 were both reached by landline, and lived in a household with more than one adult, with the majority of that subset living in households with only one other adult. If we assume respondents and non-respondents are equally likely to answer the phone, this means that the household enumeration step of the interview is only a possible source for the sex variable around an eighth of the time, which makes sex from household enumeration less of a concern. Additionally, we feel that household members are still a valuable source for gender perception, being less likely to ‘correctly’ report the sex of transgender housemates who pass very well.  

One cause for the miscoding of sex that we considered is data entry error on the part of BRFSS interviewers. Given the large number of cis respondents compared to trans respondents, a small number of cis people being mistakenly coded as trans, but coded correctly with respect to their sex, could poison the trans sample. However, other clues in the data strongly discredit data entry error as the source of miscoded sex. As shown in Figure \ref{fig:miscodedbyyear}, the share of trans people with the `wrong' sex increased sharply in 2019. The fact that the CDC's increased interest in this variable resulted in \textit{more} trans people with their sex coded `wrong', instead of fewer or the same number, indicates that these responses are legitimate, and not the result of data entry error, which we would expect to be constant or declining as the CDC and states improve their processes. Additionally, we would expect the number of erroneous entries to be proportional to the total number of respondents, rather than to the number of trans respondents, and occurring at roughly the same rate year-on-year. The number of trans men sampled more than tripled between 2014 and 2021, so if the trans men coded as male were actually cis men who were miscoded as trans, we would expect the share of trans men coded as male to shrink, not grow. 



\clearpage
\end{document}
